<?php
namespace Royalsoft\H4L_Plugin;

defined('ABSPATH') || exit;

/*
Plugin Name: H4L plugin
Description: H4L plugin
Author: Royalsoft
Version: 1.0.0
*/

//define constants
define('H4L_PLUGIN_NAME', 'h4l-plugin');
define('H4L_PLUGIN_VERSION', '1.0.0');
define('H4L_PLUGIN_BASE_FILE', plugin_basename(__FILE__));
define('H4L_PLUGIN_BASE_DIR', WP_PLUGIN_DIR.'/'.H4L_PLUGIN_NAME.'/');
define('H4L_PLUGIN_BASE_URL', plugin_dir_url(__FILE__));

$is_localhost = stripos(H4L_PLUGIN_BASE_URL, '//localhost/') !== false;

define('H4L_BOT_WP_USER_ID', $is_localhost ? 5 : 10);
define('H4L_SUPERVISOR_WP_USER_ID', $is_localhost ? 4 : 9);

//define main class
class H4L_Plugin
{
	private static $_articles = [];
	
	
	/**
	 * Inits plugin.
	 */
	public static function run()
	{
		self::$_articles = include 'res/articles.php';
		
		add_action('bp_activity_add', [__CLASS__, 'h4l_bp_activity_add'], 10, 2);
		add_action('messages_message_sent', [__CLASS__, 'h4l_messages_message_sent'], 10, 1);
		add_action('messages_message_sent', [__CLASS__, 'h4l_messages_message_sent_talk_with_bot'], 10, 1);
		add_action('h4l_activity_create_meta_uploadedmedia', [__CLASS__, 'h4l_activity_create_meta_uploadedmedia'], 10, 1);
		add_action('h4l_get_threads_thread_object', [__CLASS__, 'h4l_get_threads_thread_object'], 10, 2);
		add_action('h4l_thread_check_new_message_object', [__CLASS__, 'h4l_thread_check_new_message_object'], 10, 2);
		add_action('bp_messages_thread_post_populate', [__CLASS__, 'h4l_bp_messages_thread_post_populate'], 10, 1);
		
		add_filter('h4l_get_activities_activity', [__CLASS__, 'h4l_get_activities_activity'], 10, 1);
		add_filter('h4l_render_stack_message_array', [__CLASS__, 'h4l_render_stack_message_array'], 10, 1);
	}
	
	
	/**
	 * _get_random_article
	 */
	private static function _get_random_article()
	{
		$keys = array_keys(self::$_articles);
		$random_key = $keys[random_int(0, count($keys) - 1)];
		
		return self::$_articles[$random_key];
	}
	
	
	/**
	 * _get_user_name
	 */
	private static function _get_user_name($user_id)
	{
		$wp_user = get_user_by('id', $user_id);
		
		return $wp_user->display_name;
	}
	
	
	/**
	 * _get_current_user_id
	 */
	private static function _get_current_user_id()
	{
		$wp_user = wp_get_current_user();
		
		return absint($wp_user->ID);
	}
	
	
	/**
	 * _is_current_supervisor
	 */
	private static function _is_current_supervisor()
	{
		return absint(self::_get_current_user_id()) === absint(H4L_SUPERVISOR_WP_USER_ID);
	}
	
	
	/**
	 * _insert_log_activity
	 */
	private static function _insert_log_activity($activity_id, array $data)
	{
		global $wpdb;
		
		$data['activity_id'] = $activity_id;
		$data['remote_ip'] = $_SERVER['REMOTE_ADDR'];
		
		$wpdb->insert($wpdb->prefix.'h4l_log_activities', $data);
		
		return $wpdb->insert_id;
	}
	
	
	/**
	 * _update_log_activity_entry
	 */
	private static function _update_log_activity_entry($log_id, array $data)
	{
		global $wpdb;
		
		return $wpdb->update($wpdb->prefix.'h4l_log_activities', $data, ['log_id' => $log_id]);
	}
	
	
	/**
	 * _insert_log_message
	 */
	private static function _insert_log_message($thread_id, $message_id, array $data)
	{
		global $wpdb;
		
		$data['thread_id'] = $thread_id;
		$data['message_id'] = $message_id;
		$data['remote_ip'] = $_SERVER['REMOTE_ADDR'];
		
		$wpdb->insert($wpdb->prefix.'h4l_log_messages', $data);
		
		return $wpdb->insert_id;
	}
	
	/**
	 * _update_log_message_entry
	 */
	private static function _update_log_message_entry($log_id, array $data)
	{
		global $wpdb;
		
		return $wpdb->update($wpdb->prefix.'h4l_log_messages', $data, ['log_id' => $log_id]);
	}
	
	
	/**
	 * _get_log_activity_by_log_id
	 */
	private static function _get_log_activity_by_log_id($log_id)
	{
		global $wpdb;
		
		return $wpdb->get_row(
			$wpdb->prepare('SELECT * FROM '.$wpdb->prefix.'h4l_log_activities WHERE log_id = %d LIMIT 1', $log_id)
		);
	}
	
	
	/**
	 * _get_log_activity
	 */
	private static function _get_log_activity($activity_id)
	{
		global $wpdb;
		
		return $wpdb->get_row(
			$wpdb->prepare('SELECT * FROM '.$wpdb->prefix.'h4l_log_activities WHERE activity_id = %d LIMIT 1', $activity_id)
		);
	}
	
	
	/**
	 * _get_log_message_by_log_id
	 */
	private static function _get_log_message_by_log_id($log_id)
	{
		global $wpdb;
		
		return $wpdb->get_row(
			$wpdb->prepare('SELECT * FROM '.$wpdb->prefix.'h4l_log_messages WHERE log_id = %d LIMIT 1', $log_id)
		);
	}
	
	
	/**
	 * _get_log_message
	 */
	private static function _get_log_message($message_id)
	{
		global $wpdb;
		
		return $wpdb->get_row(
			$wpdb->prepare('SELECT * FROM '.$wpdb->prefix.'h4l_log_messages WHERE message_id = %d LIMIT 1', $message_id)
		);
	}
	
	
	/**
	 * _get_any_flagged_log_message_for_thread
	 */
	private static function _get_any_flagged_log_message_for_thread($thread_id)
	{
		global $wpdb;
		
		return $wpdb->get_row(
			$wpdb->prepare('SELECT * FROM '.$wpdb->prefix.'h4l_log_messages WHERE thread_id = %d AND (is_hard = 1 OR is_soft = 1) LIMIT 1', $thread_id)
		);
	}
	
	
	/**
	 * _check_forbidden_phrases
	 */
	private static function _check_forbidden_phrases($str)
	{
		$phrases = include 'dict/phrases.php';
		
		foreach($phrases as $p) {
			if(mb_stripos($str, trim($p)) !== false) {
				return true;
			}
		}
		
		return false;
	}
	
	
	/**
	 * _check_forbidden_urls
	 */
	private static function _check_forbidden_urls($str)
	{
		$urls = include 'dict/urls.php';
		
		foreach($urls as $p) {
			if(mb_stripos($str, trim($p)) !== false) {
				return true;
			}
		}
		
		return false;
	}
	
	
	/**
	 * _check_forbidden_emojis
	 */
	private static function _check_forbidden_emojis($str)
	{
		$emojis = include 'dict/emojis.php';
		
		foreach($emojis as $p) {
			if(mb_stripos($str, $p) !== false) {
				return true;
			}
		}
		
		return false;
	}
	
	
	/**
	 * _check_forbidden_images
	 */
	private static function _check_forbidden_images($link)
	{
		$images = include 'dict/images.php';
		$content_hash = sha1(file_get_contents($link));
		
		foreach($images as $p) {
			if($content_hash === $p) {
				return true;
			}
		}
		
		return false;
	}
	
	
	/**
	 * _check_length
	 */
	private static function _check_length($str)
	{
		return mb_strlen($str) > 200;
	}
	
	
	/**
	 * _check_caps
	 */
	private static function _check_caps($str)
	{
		$ignore = [
			'<!-- BBPM START THREAD -->',
		];
		
		foreach($ignore as $i) {
			if($str === $i) {
				return false;
			}
		}
		
		$total_len = mb_strlen($str);
		
		if(!$total_len) {
			return false;
		}
		
		$caps_len = 0;
		$matches = [];
		
		if(preg_match_all('/[A-Z]/', $str, $matches) > 0) {
			$caps_len = count($matches[0]);
		}
		
		return $caps_len > 0 && (100 * ($caps_len / $total_len)) > 33;
	}
	
	
	/**
	 * _check_exclamations
	 */
	private static function _check_exclamations($str)
	{
		$excl_len = 0;
		$matches = [];
		
		if(preg_match_all('/\!/', $str, $matches) > 0) {
			$excl_len = count($matches[0]);
		}
		
		return $excl_len > 10;
	}
	
	
	/**
	 * _check_emoji_stream
	 */
	private static function _check_emoji_stream($str)
	{
		$emojis_len = 0;
		$matches = [];
		
		if(preg_match_all('/(\:[a-z0-9]+\:|\&\#x[a-f0-9]+\;){10,}/', $str, $matches) > 0) {
			$emojis_len = count($matches[0]);
		}
		
		return $emojis_len > 0;
	}
	
	
	/**
	 * _send_warning_message
	 */
	private static function _send_warning_message($type, $recipient_user_id, $attacker_user_id, array $meta)
	{
		$content = '';
		
		if(isset($attacker_user_id)) {
			$attacker_display_name = self::_get_user_name($attacker_user_id);
		}else{
			$attacker_display_name = 'unknown';
		}
		
		switch($type) {
			case 'activity_hard_detected_attacker':
				$subject = 'Warning!';
				$content = 'The content you posted has been blocked. Using offensive language is not allowed. This incident has been logged.';
				$content .= "\n\r".'Available commands: "why", "help".';
				break;
				
			case 'activity_soft_detected_attacker':
				$subject = 'Be careful!';
				$content = 'The content you posted can be misinterpreted. There is a high chance that someone is offended now. This incident has been logged.';
				$content .= "\n\r".'Available commands: "why", "help".';
				break;
				
			case 'activity_hard_detected_supervisor':
				$subject = 'Pay attention';
				$content = 'User "'.$attacker_display_name.'" posted content with offensive language.';
				$content .= "\n\r".'Available commands: "why", "show", "accept", "delete".';
				break;
				
			case 'activity_soft_detected_supervisor':
				$subject = 'Pay attention';
				$content = 'User "'.$attacker_display_name.'" posted content with an ambiguous meaning marked by negative emotions.';
				$content .= "\n\r".'Available commands: "why", "show", "accept", "delete".';
				break;
				
			case 'activity_hard_detected_victim':
				$subject = 'Do you need help?';
				$content = 'Someone is trying to offend you by posting offensive content. Please let us know if you are feeling unsafe and require professional help.';
				$content .= "\n\r".'Available commands: "why", "help".';
				break;
				
			case 'activity_soft_detected_victim':
				$subject = 'Do you want to talk?';
				$content = 'Someone is trying to offend you by posting content with ambiguous meaning. Please let us know if you are feeling unsafe and require professional help.';
				$content .= "\n\r".'Available commands: "why", "help".';
				break;
				
			case 'message_hard_detected_attacker':
				$subject = 'Warning!';
				$content = 'The message you sent has been blocked. Using offensive language is not allowed. This incident has been logged.';
				$content .= "\n\r".'Available commands: "why", "help".';
				break;
				
			case 'message_soft_detected_attacker':
				$subject = 'Be careful!';
				$content = 'The message you sent can be misinterpreted. There is a high chance that someone is offended now. This incident has been logged.';
				$content .= "\n\r".'Available commands: "why", "help".';
				break;
				
			case 'message_hard_detected_supervisor':
				$subject = 'Pay attention';
				$content = 'User "'.$attacker_display_name.'" sent a message with offensive language.';
				$content .= "\n\r".'Available commands: "why", "show", "accept", "delete".';
				break;
				
			case 'message_soft_detected_supervisor':
				$subject = 'Pay attention';
				$content = 'User "'.$attacker_display_name.'" sent a message with an ambiguous meaning marked by negative emotions.';
				$content .= "\n\r".'Available commands: "why", "show", "accept", "delete".';
				break;
				
			case 'message_hard_detected_victim':
				$subject = 'Do you need help?';
				$content = 'Someone is trying to offend you by sending you offensive messages. Please let us know if you are feeling unsafe and require professional help.';
				$content .= "\n\r".'Available commands: "why", "help".';
				break;
				
			case 'message_soft_detected_victim':
				$subject = 'Do you want to talk?';
				$content = 'Someone is trying to offend you by sending you messages with ambiguous meaning. Please let us know if you are feeling unsafe and require professional help.';
				$content .= "\n\r".'Available commands: "why", "help".';
				break;
		}
		
		$args = [
			'sender_id' => H4L_BOT_WP_USER_ID,
			'subject' => $subject,
			'content' => \BP_Better_Messages()->functions->filter_message_content($content),
			'error_type' => 'wp_error',
			'append_thread' => false,
			'recipients' => [$recipient_user_id],
		];
		
		$thread_id = messages_new_message($args);
		$messages = \BP_Messages_Thread::get_messages($thread_id);
		$first_message_id = $messages[0]->id;
		
		if(!empty($meta)) {
			foreach($meta as $key => $value) {
				bp_messages_add_meta($first_message_id, $key, $value);
			}
		}
	}
	
	
	/**
	 * _send_bot_response_message_activity
	 */
	private static function _send_bot_response_message_activity($type, $recipient_user_id, $thread_id, $log_id)
	{
		$content = '';
		
		$log = self::_get_log_activity_by_log_id($log_id);
		
		if(!is_object($log)) {
			return;
		}
		
		//get activity
		$activity = new \BP_Activity_Activity($log->activity_id);
		
		if(empty($activity->id)) {
			return;
		}
		
		//get related activity
		$rel_activity = new \BP_Activity_Activity($activity->item_id);
		
		switch($type) {
			case 'why190':
			case 'why190a':
			case 'why212':
			case 'why265':
			case 'why216':
			case 'why207':
			case 'why':
				if($type === 'why') {
					$why = self::_get_random_article();
				}else{
					$why = self::$_articles[$type];
				}
				
				//attacker
				if(self::_get_current_user_id() === absint($activity->user_id)) {
					$content = 'You may have committed an offense under the article: '.$why;
				}
				
				//victim
				else if(!empty($rel_activity->id) && self::_get_current_user_id() === absint($rel_activity->user_id)) {
					$content = 'It is likely that someone has committed an offence against you under the article: '.$why;
				}
				
				//supervisor
				else {
					$content = 'It is likely that the autor of the content has committed an offence under the article: '.$why;
				}
				break;
			
			case 'help':
				//attacker
				if(self::_get_current_user_id() === absint($activity->user_id)) {
					$content = 'Our supervisor '.self::_get_user_name(H4L_SUPERVISOR_WP_USER_ID).' will contact you to clarify the situation. In the meantime, please get familiar with the message from the local authorities:
						Pamiętaj, jeśli potrzebujesz pomocy, znajdziesz ją tutaj:
						https://www.nieobrazsieale.pl/
						http://hejtstop.pl/
						Poinformuj lokalny komisariat Policji tel. 112
					';
				}
				
				//victim
				else if(!empty($rel_activity->id) && self::_get_current_user_id() === absint($rel_activity->user_id)) {
					$content = 'If you need personal support, please contact our supervisor '.self::_get_user_name(H4L_SUPERVISOR_WP_USER_ID).'. Also get familiar with the message from the local authorities:
						Pamiętaj, jeśli potrzebujesz pomocy, znajdziesz ją tutaj:
						https://www.nieobrazsieale.pl/
						http://hejtstop.pl/
						Poinformuj lokalny komisariat Policji tel. 112
					';
				}
				
				//supervisor
				else {
					$content = 'Message from the local authorities:
						Pamiętaj, jeśli potrzebujesz pomocy, znajdziesz ją tutaj:
						https://www.nieobrazsieale.pl/
						http://hejtstop.pl/
						Poinformuj lokalny komisariat Policji tel. 112
					';
				}
				break;
			
			case 'show':
				$content = 'Type: '.($log->is_hard == 1 ? '*** Dangerous content detected ***' : 'Suspicious content detected');
				$content .= "\n\r".'Sent from IP: '.$log->remote_ip;
				$content .= "\n\r".'Preview: '.mb_substr(trim($activity->content), 0, 50);
				$content .= "\n\r".'Link to content: '.$activity->primary_link;
				break;
			
			case 'accept':
				self::_update_log_activity_entry($log->log_id, ['is_hard' => 0, 'is_soft' => 0]);
				
				$content = 'Content has been accepted and unlocked.';
				break;
			
			case 'delete':
				\BP_Activity_Activity::delete(['id' => $activity->id]);
				self::_update_log_activity_entry($log->log_id, ['is_deleted' => 1]);
				
				$content = 'Content has been deleted.';
				break;
		}
		
		$args = [
			'sender_id' => H4L_BOT_WP_USER_ID,
			'thread_id' => $thread_id,
			//'subject' => $subject,
			'content' => \BP_Better_Messages()->functions->filter_message_content($content),
			'error_type' => 'wp_error',
			//'append_thread' => false,
			'recipients' => [$recipient_user_id],
		];
		
		messages_new_message($args);
	}
	
	
	/**
	 * _send_bot_response_message_message
	 */
	private static function _send_bot_response_message_message($type, $recipient_user_id, $thread_id, $log_id)
	{
		$content = '';
		
		$log = self::_get_log_message_by_log_id($log_id);
		
		if(!is_object($log)) {
			return;
		}
		
		//get message
		$message = new \BP_Messages_Message($log->message_id);
		
		if(empty($message->id)) {
			return;
		}
		
		$recipients = $message->get_recipients();
		$recipients_ids = [];
		$recipients_names = [];
		
		if(is_array($recipients)) {
			foreach($recipients as $r) {
				if($r->user_id != $message->sender_id) {
					$recipients_ids[$r->user_id] = true;
					$recipients_names[] = H4L_Plugin::_get_user_name($r->user_id);
				}
			}
		}
		
		switch($type) {
			case 'why190':
			case 'why190a':
			case 'why212':
			case 'why265':
			case 'why216':
			case 'why207':
			case 'why':
				if($type === 'why') {
					$why = self::_get_random_article();
				}else{
					$why = self::$_articles[$type];
				}
				
				//attacker
				if(self::_get_current_user_id() === absint($message->sender_id)) {
					$content = 'You may have committed an offense under the article: '.$why;
				}
				
				//victim
				else if(isset($recipients_ids[self::_get_current_user_id()])) {
					$content = 'It is likely that someone has committed an offence against you under the article: '.$why;
				}
				
				//supervisor
				else {
					$content = 'It is likely that the autor of the message has committed an offence under the article: '.$why;
				}
				break;
			
			case 'help':
				//attacker
				if(self::_get_current_user_id() === absint($message->sender_id)) {
					$content = 'Our supervisor '.self::_get_user_name(H4L_SUPERVISOR_WP_USER_ID).' will contact you to clarify the situation. In the meantime, please get familiar with the message from the local authorities:
						Pamiętaj, jeśli potrzebujesz pomocy, znajdziesz ją tutaj:
						https://www.nieobrazsieale.pl/
						http://hejtstop.pl/
						Poinformuj lokalny komisariat Policji tel. 112
					';
				}
				
				//victim
				if(isset($recipients_ids[self::_get_current_user_id()])) {
					$content = 'If you need personal support, please contact our supervisor '.self::_get_user_name(H4L_SUPERVISOR_WP_USER_ID).'. Also get familiar with the message from the local authorities:
						Pamiętaj, jeśli potrzebujesz pomocy, znajdziesz ją tutaj:
						https://www.nieobrazsieale.pl/
						http://hejtstop.pl/
						Poinformuj lokalny komisariat Policji tel. 112
					';
				}
				
				//supervisor
				else {
					$content = 'Message from the local authorities:
						Pamiętaj, jeśli potrzebujesz pomocy, znajdziesz ją tutaj:
						https://www.nieobrazsieale.pl/
						http://hejtstop.pl/
						Poinformuj lokalny komisariat Policji tel. 112
					';
				}
				break;
			
			case 'show':
				$content = 'Type: '.($log->is_hard == 1 ? '*** Dangerous content detected ***' : 'Suspicious content detected');
				$content .= "\n\r".'Sent from IP: '.$log->remote_ip;
				$content .= "\n\r".'Sender: '.self::_get_user_name($message->sender_id);
				$content .= "\n\r".'Recipients: '.implode(', ' , $recipients_names);
				$content .= "\n\r".'Subject: '.trim($message->subject);
				$content .= "\n\r".'Full content: '.trim($message->message);
				break;
			
			case 'accept':
				self::_update_log_message_entry($log->log_id, ['is_hard' => 0, 'is_soft' => 0]);
				
				$content = 'Message has been accepted and unlocked.';
				break;
			
			case 'delete':
				global $wpdb;
        $wpdb->query($wpdb->prepare("DELETE FROM " . bpbm_get_table('messages') . " WHERE id = %d", $message->id));
        $wpdb->query($wpdb->prepare("DELETE FROM " . bpbm_get_table('meta') . " WHERE message_id = %d", $message->id));

				self::_update_log_message_entry($log->log_id, ['is_deleted' => 1]);
				
				$content = 'Message has been deleted.';
				break;
		}
		
		$args = [
			'sender_id' => H4L_BOT_WP_USER_ID,
			'thread_id' => $thread_id,
			//'subject' => $subject,
			'content' => \BP_Better_Messages()->functions->filter_message_content($content),
			'error_type' => 'wp_error',
			//'append_thread' => false,
			'recipients' => [$recipient_user_id],
		];
		
		messages_new_message($args);
	}
	
	
	/**
	 * bp_activity_add
	 */
	public static function h4l_bp_activity_add($r, $activity_id)
	{
		if(self::_is_current_supervisor()) {
			return;
		}
		
		//get activity
		$activity = new \BP_Activity_Activity($activity_id);
		
		if(empty($activity->id)) {
			return;
		}
		
		$is_hard = false;
		$is_soft = false;
		$reasons = [];
		
		//choose activity by component
		switch($activity->component) {
			case 'activity':
				//choose activity by type
				switch($activity->type) {
					case 'activity_update':	//post added
					case 'activity_comment':	//comment added
						//get related activity
						$rel_activity = new \BP_Activity_Activity($activity->item_id);
						
						//detect phrases
						if(self::_check_forbidden_phrases($activity->content)) {
							$is_hard = true;
							$reasons['phrases'] = true;
						}
						
						//detect urls
						if(self::_check_forbidden_urls($activity->content)) {
							$is_hard = true;
							$reasons['urls'] = true;
						}
						
						//detect emojis
						if(self::_check_forbidden_emojis($activity->content)) {
							$is_hard = true;
							$reasons['emojis'] = true;
						}
						
						//detect length
						if(self::_check_length($activity->content)) {
							$is_soft = true;
							$reasons['length'] = true;
						}
						
						//detect CAPS
						if(self::_check_caps($activity->content)) {
							$is_soft = true;
							$reasons['caps'] = true;
						}
						
						//detect exclamations
						if(self::_check_exclamations($activity->content)) {
							$is_soft = true;
							$reasons['exclamations'] = true;
						}
						
						//detect emoji stream
						if(self::_check_emoji_stream($activity->content)) {
							$is_soft = true;
							$reasons['emoji_stream'] = true;
						}
						
						//log
						if($is_hard || $is_soft) {
							$log_id = self::_insert_log_activity($activity->id, [
								'is_hard' => $is_hard ? 1 : 0,
								'is_soft' => $is_soft ? 1 : 0,
								'reasons' => json_encode($reasons),
							]);
						}
						
						//send messages
						if($is_hard) {
							$meta = ['h4l_log_id' => $log_id, 'h4l_log_type' => 'activity'];
							
							self::_send_warning_message('activity_hard_detected_attacker', $activity->user_id, $activity->user_id, $meta);
							self::_send_warning_message('activity_hard_detected_supervisor', H4L_SUPERVISOR_WP_USER_ID, $activity->user_id, $meta);
							
							if(!empty($rel_activity->id)) {
								self::_send_warning_message('activity_hard_detected_victim', $rel_activity->user_id, $activity->user_id, $meta);
							}
						}else if($is_soft) {
							$meta = ['h4l_log_id' => $log_id, 'h4l_log_type' => 'activity'];
							
							self::_send_warning_message('activity_soft_detected_attacker', $activity->user_id, $activity->user_id, $meta);
							self::_send_warning_message('activity_soft_detected_supervisor', H4L_SUPERVISOR_WP_USER_ID, $activity->user_id, $meta);
							
							if(!empty($rel_activity->id)) {
								self::_send_warning_message('activity_soft_detected_victim', $rel_activity->user_id, $activity->user_id, $meta);
							}
						}
						break;
				}
				break;
		}
	}
	
	
	/**
	 * h4l_activity_create_meta_uploadedmedia
	 */
	public static function h4l_activity_create_meta_uploadedmedia($activity_id)
	{
		if(self::_is_current_supervisor()) {
			return;
		}
		
		//get activity
		$activity = new \BP_Activity_Activity($activity_id);
		
		if(empty($activity->id)) {
			return;
		}
		
		$is_hard = false;
		$reasons = [];
		
		//get meta
		$uploaded_media_ids = bp_activity_get_meta($activity->id, 'uploaded_media_id', false);	//array
		
		if(empty($uploaded_media_ids)) {
			return;
		}
		
		foreach($uploaded_media_ids as $uploaded_media_id) {
			$media = vkmedia_get_media($uploaded_media_id);
			
			if(!is_object($media)) {
				continue;
			}
			
			//detect images
			if(self::_check_forbidden_images($media->link)) {
				$is_hard = true;
				$reasons['images'] = true;
			}
		}
		
		//log
		if($is_hard) {
			$log_id = self::_insert_log_activity($activity->id, [
				'is_hard' => 1,
				'reasons' => json_encode($reasons),
			]);
			
			$meta = ['h4l_log_id' => $log_id, 'h4l_log_type' => 'activity'];
			
			self::_send_warning_message('activity_hard_detected_attacker', $activity->user_id, $activity->user_id, $meta);
			self::_send_warning_message('activity_hard_detected_supervisor', H4L_SUPERVISOR_WP_USER_ID, $activity->user_id, $meta);
		}
	}
	
	
	/**
	 * h4l_messages_message_sent
	 */
	public static function h4l_messages_message_sent($message)
	{
		if(self::_is_current_supervisor()) {
			return;
		}
		
		if(!is_object($message)) {
			return;
		}
		
		if(absint($message->sender_id) === H4L_BOT_WP_USER_ID) {
			return;
		}
		
		$is_hard = false;
		$is_soft = false;
		$reasons = [];
		
		//detect phrases
		if(self::_check_forbidden_phrases($message->subject) || self::_check_forbidden_phrases($message->message)) {
			$is_hard = true;
			$reasons['phrases'] = true;
		}
		
		//detect urls
		if(self::_check_forbidden_urls($message->subject) || self::_check_forbidden_urls($message->message)) {
			$is_hard = true;
			$reasons['urls'] = true;
		}
		
		//detect emojis
		if(self::_check_forbidden_emojis($message->subject) || self::_check_forbidden_emojis($message->message)) {
			$is_hard = true;
			$reasons['emojis'] = true;
		}
		
		//detect length
		if(self::_check_length($message->subject) || self::_check_length($message->message)) {
			$is_soft = true;
			$reasons['length'] = true;
		}
		
		//detect CAPS
		if(self::_check_caps($message->subject) || self::_check_caps($message->message)) {
			$is_soft = true;
			$reasons['caps'] = true;
		}
		
		//detect exclamations
		if(self::_check_exclamations($message->subject) || self::_check_exclamations($message->message)) {
			$is_soft = true;
			$reasons['exclamations'] = true;
		}
		
		//detect emoji stream
		if(self::_check_emoji_stream($message->subject) || self::_check_emoji_stream($message->message)) {
			$is_soft = true;
			$reasons['emoji_stream'] = true;
		}
		
		//log
		if($is_hard || $is_soft) {
			$log_id = self::_insert_log_message($message->thread_id, $message->id, [
				'is_hard' => $is_hard ? 1 : 0,
				'is_soft' => $is_soft ? 1 : 0,
				'reasons' => json_encode($reasons),
			]);
		}
		
		//send messages
		if($is_hard) {
			$meta = ['h4l_log_id' => $log_id, 'h4l_log_type' => 'message'];
			
			self::_send_warning_message('message_hard_detected_attacker', $message->sender_id, $message->sender_id, $meta);
			self::_send_warning_message('message_hard_detected_supervisor', H4L_SUPERVISOR_WP_USER_ID, $message->sender_id, $meta);
			
			foreach($message->recipients as $recipient) {
				if(is_object($recipient) && isset($recipient->user_id)) {
					self::_send_warning_message('message_hard_detected_victim', $recipient->user_id, $message->sender_id, $meta);
				}
			}
		}else if($is_soft) {
			$meta = ['h4l_log_id' => $log_id, 'h4l_log_type' => 'message'];
			
			self::_send_warning_message('message_soft_detected_attacker', $message->sender_id, $message->sender_id, $meta);
			self::_send_warning_message('message_soft_detected_supervisor', H4L_SUPERVISOR_WP_USER_ID, $message->sender_id, $meta);
			
			foreach($message->recipients as $recipient) {
				if(is_object($recipient) && isset($recipient->user_id)) {
					self::_send_warning_message('message_soft_detected_victim', $recipient->user_id, $message->sender_id, $meta);
				}
			}
		}
	}
	
	
	/**
	 * h4l_messages_message_sent_talk_with_bot
	 */
	public static function h4l_messages_message_sent_talk_with_bot($message)
	{
		if(!is_object($message)) {
			return;
		}
		
		//get log
		$messages = \BP_Messages_Thread::get_messages($message->thread_id);
		$first_message_id = $messages[0]->id;
		$log_id = bp_messages_get_meta($first_message_id, 'h4l_log_id');
		$log_type = bp_messages_get_meta($first_message_id, 'h4l_log_type');
		
		if(empty($log_id) || empty($log_type)) {
			return;
		}
		
		//supervisor
		if(self::_is_current_supervisor()) {
			switch($log_type) {
				case 'activity':
					switch(trim($message->message)) {
						case 'show':
							self::_send_bot_response_message_activity('show', self::_get_current_user_id(), $message->thread_id, $log_id);
							break;
						
						case 'accept':
							self::_send_bot_response_message_activity('accept', self::_get_current_user_id(), $message->thread_id, $log_id);
							break;
						
						case 'delete':
							self::_send_bot_response_message_activity('delete', self::_get_current_user_id(), $message->thread_id, $log_id);
							break;
					}
					break;
				
				case 'message':
					switch(trim($message->message)) {
						case 'show':
							self::_send_bot_response_message_message('show', self::_get_current_user_id(), $message->thread_id, $log_id);
							break;
						
						case 'accept':
							self::_send_bot_response_message_message('accept', self::_get_current_user_id(), $message->thread_id, $log_id);
							break;
						
						case 'delete':
							self::_send_bot_response_message_message('delete', self::_get_current_user_id(), $message->thread_id, $log_id);
							break;
					}
					break;
			}
		}
		
		//all
		switch($log_type) {
			case 'activity':
				switch(trim($message->message)) {
					case 'why190':
					case 'why190a':
					case 'why212':
					case 'why265':
					case 'why216':
					case 'why207':
					case 'why':
						self::_send_bot_response_message_activity(trim($message->message), self::_get_current_user_id(), $message->thread_id, $log_id);
						break;
					
					case 'help':
						self::_send_bot_response_message_activity('help', self::_get_current_user_id(), $message->thread_id, $log_id);
						break;
				}
				break;
						
			case 'message':
				switch(trim($message->message)) {
					case 'why190':
					case 'why190a':
					case 'why212':
					case 'why265':
					case 'why216':
					case 'why207':
					case 'why':
						self::_send_bot_response_message_message(trim($message->message), self::_get_current_user_id(), $message->thread_id, $log_id);
						break;
					
					case 'help':
						self::_send_bot_response_message_message('help', self::_get_current_user_id(), $message->thread_id, $log_id);
						break;
				}
				break;
		}
	}
	
	
	/**
	 * h4l_get_activities_activity
	 */
	public static function h4l_get_activities_activity($activity)
	{
		if(self::_is_current_supervisor()) {
			return $activity;
		}
		
		if(!is_array($activity)) {
			return $activity;
		}
		
		$log = self::_get_log_activity($activity['id']);
		
		if(!is_object($log)) {
			return $activity;
		}
		
		if($log->is_hard) {
			$activity['content'] = '------------------------------------ BLOCKED ------------------------------------';
			$activity['content'] .= "<br />".'This content has been blocked.';
			$activity['content'] .= "<br />".'Offensive language has been detected.';
			$activity['content'] .= "<br />".'Investigation in progress.';
			$activity['content'] .= "<br />".'------------------------------------ BLOCKED ------------------------------------';
			
			if(isset($activity['uploaded_media']) && is_array($activity['uploaded_media'])
				&& isset($activity['uploaded_media']['data']) && is_array($activity['uploaded_media']['data'])
			) {
				foreach($activity['uploaded_media']['data'] as $data) {
					if(isset($data['media']) && is_object($data['media'])) {
						$data['media']->name = 'Blocked content';
						$data['media']->link = 'http://solaris.royalsoft.pl/wp-content/uploads/2021/06/blocked-content-placeholder.png';
					}
				}
			}
		}
		
		return $activity;
	}
	
	
	/**
	 * h4l_get_threads_thread_object
	 */
	public static function h4l_get_threads_thread_object($thread, $new_thread)
	{
		if(self::_is_current_supervisor()) {
			return;
		}
		
		if(!is_object($thread) || !is_object($new_thread)) {
			return;
		}
		
		$log = self::_get_any_flagged_log_message_for_thread($thread->thread_id);
		
		if(!is_object($log)) {
			return;
		}
		
		if($log->is_hard) {
			$new_thread->subject = '--- BLOCKED ---';
			$new_thread->message = 'Messages in this thread have been blocked.';
		}
	}
	
	
	/**
	 * h4l_render_stack_message_array
	 */
	public static function h4l_render_stack_message_array($message)
	{
		if(self::_is_current_supervisor()) {
			return $message;
		}
		
		if(!is_array($message)) {
			return $message;
		}
		
		$log = self::_get_log_message($message['id']);
		
		if(!is_object($log)) {
			return $message;
		}
		
		if($log->is_hard) {
			$message['message'] = '------------------------------------ BLOCKED ------------------------------------';
			$message['message'] .= "<br />".'This message has been blocked.';
			$message['message'] .= "<br />".'Offensive language has been detected.';
			$message['message'] .= "<br />".'Investigation in progress.';
			$message['message'] .= "<br />".'------------------------------------ BLOCKED ------------------------------------';
		}
		
		return $message;
	}
	
	
	/**
	 * h4l_thread_check_new_message_object
	 */
	public static function h4l_thread_check_new_message_object($message, $new_message)
	{
		if(self::_is_current_supervisor()) {
			return;
		}
		
		if(!is_object($message) || !is_object($new_message)) {
			return;
		}
		
		$log = self::_get_log_message($message->id);
		
		if(!is_object($log)) {
			return;
		}
		
		if($log->is_hard) {
			$new_message->message = '------------------------------------ BLOCKED ------------------------------------';
			$new_message->message .= "<br />".'This message has been blocked.';
			$new_message->message .= "<br />".'Offensive language has been detected.';
			$new_message->message .= "<br />".'Investigation in progress.';
			$new_message->message .= "<br />".'------------------------------------ BLOCKED ------------------------------------';
		}
	}
	
	
	/**
	 * h4l_bp_messages_thread_post_populate
	 */
	public static function h4l_bp_messages_thread_post_populate($thread)
	{
		if(self::_is_current_supervisor()) {
			return;
		}
		
		if(!is_object($thread)) {
			return;
		}
		
		$log = self::_get_any_flagged_log_message_for_thread($thread->thread_id);
		
		if(!is_object($log)) {
			return;
		}
		
		if($log->is_hard) {
			$thread->last_message_subject = '--- BLOCKED ---';
			$thread->last_message_content = 'Messages in this thread have been blocked.';
		}
	}
}

//run plugin
H4L_Plugin::run();
