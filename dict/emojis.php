<?php

return [
	'😡',
	'😠',
	'👿',
	'💩',
	'🔪',
	'🗡',
	'️',
	'⚔',
	'️',
	'🖕',
	'🏻',
	'🖕',
	'🏼',
	'🖕',
	'🏽',
	'🖕',
	'🏾',
	'🖕',
	'🏿',
	'🤬',
	'🖕',
	'🔫',
	'🤮',
	'🤢',
	'💣',
	
	'&#x1f621;',
	'&#x1f620;',
	'&#x1f47f;',
	'&#x1f4a9;',
	'&#x1f52a;',
	'&#x1f5e1;',
	'&#xfe0f;',
	'&#x2694;',
	'&#xfe0f;',
	'&#x1f595;',
	'&#x1f3fb;',
	'&#x1f595;',
	'&#x1f3fc;',
	'&#x1f595;',
	'&#x1f3fd;',
	'&#x1f595;',
	'&#x1f3fe;',
	'&#x1f595;',
	'&#x1f3ff;',
	'&#x1f92c;',
	'&#x1f595;',
	'&#x1f52b;',
	'&#x1f92e;',
	'&#x1f922;',
	'&#x1f4a3;',
	
	':rage:',
	':angry:',
	':imp:',
	':poop:',
	':knife:',
	':dagger:',
	':crossed_swords:',
	':middle_finger_tone1:',
	':middle_finger_tone2:',
	':middle_finger_tone3:',
	':middle_finger_tone4:',
	':middle_finger_tone5:',
	':face_with_symbols_over_mouth:',
	':middle_finger:',
	':gun:',
	':face_vomiting:',
	':nauseated_face:',
	':bomb:',
];