<?php

return [
	'why190' => '
		Art.  190.  [Groźba karalna]
		§  1. 
		Kto grozi innej osobie popełnieniem przestępstwa na jej szkodę lub szkodę osoby najbliższej, jeżeli groźba wzbudza w zagrożonym uzasadnioną obawę, że będzie spełniona,
		podlega grzywnie, karze ograniczenia wolności albo pozbawienia wolności do lat 2.
	',
	
	'why190a' => '
		Art.  190a.  [Uporczywe nękanie. Kradzież tożsamości]
		§  1. 
		Kto przez uporczywe nękanie innej osoby lub osoby jej najbliższej wzbudza u niej uzasadnione okolicznościami poczucie zagrożenia, poniżenia lub udręczenia lub istotnie narusza jej prywatność, podlega karze pozbawienia wolności od 6 miesięcy do lat 8.
		
		§  2. 
		Tej samej karze podlega, kto, podszywając się pod inną osobę, wykorzystuje jej wizerunek, inne jej dane osobowe lub inne dane, za pomocą których jest ona publicznie identyfikowana, w celu wyrządzenia jej szkody majątkowej lub osobistej.
		
		§  3. 
		Jeżeli następstwem czynu określonego w § 1 lub 2 jest targnięcie się pokrzywdzonego na własne życie, sprawca podlega karze pozbawienia wolności od lat 2 do 12.
	',
	
	'why212' => '
		Art.  212.  [Zniesławienie]
		§  1. 
		Kto pomawia inną osobę, grupę osób, instytucję, osobę prawną lub jednostkę organizacyjną niemającą osobowości prawnej o takie postępowanie lub właściwości, które mogą poniżyć ją w opinii publicznej lub narazić na utratę zaufania potrzebnego dla danego stanowiska, zawodu lub rodzaju działalności, podlega grzywnie albo karze ograniczenia wolności.
		
		§  2. 
		Jeżeli sprawca dopuszcza się czynu określonego w § 1 za pomocą środków masowego komunikowania, podlega grzywnie, karze ograniczenia wolności albo pozbawienia wolności do roku.
		
		§  3. 
		W razie skazania za przestępstwo określone w § 1 lub 2 sąd może orzec nawiązkę na rzecz pokrzywdzonego, Polskiego Czerwonego Krzyża albo na inny cel społeczny wskazany przez pokrzywdzonego.
	',
	
	'why265' => '
		Art.  265.  [Ujawnianie lub wykorzystanie informacji niejawnych]
		§  1. 
		Kto ujawnia lub wbrew przepisom ustawy wykorzystuje informacje niejawne o klauzuli "tajne" lub "ściśle tajne", podlega karze pozbawienia wolności od 3 miesięcy do lat 5.
		
		§  2. 
		Jeżeli informację określoną w § 1 ujawniono osobie działającej w imieniu lub na rzecz podmiotu zagranicznego, sprawca podlega karze pozbawienia wolności od 6 miesięcy do lat 8.
		
		§  3. 
		Kto nieumyślnie ujawnia informację określoną w § 1, z którą zapoznał się w związku z pełnieniem funkcji publicznej lub otrzymanym upoważnieniem, podlega grzywnie, karze ograniczenia wolności albo pozbawienia wolności do roku.
	',
	
	'why216' => '
		Art.  216.  [Zniewaga]
		§  1. 
		Kto znieważa inną osobę w jej obecności albo choćby pod jej nieobecność, lecz publicznie lub w zamiarze, aby zniewaga do osoby tej dotarła,
		podlega grzywnie albo karze ograniczenia wolności.

		§  2. 
		Kto znieważa inną osobę za pomocą środków masowego komunikowania,
		podlega grzywnie, karze ograniczenia wolności albo pozbawienia wolności do roku.

		§  3. 
		Jeżeli zniewagę wywołało wyzywające zachowanie się pokrzywdzonego albo jeżeli pokrzywdzony odpowiedział naruszeniem nietykalności cielesnej lub zniewagą wzajemną, sąd może odstąpić od wymierzenia kary.
		
		§  4. 
		W razie skazania za przestępstwo określone w § 2 sąd może orzec nawiązkę na rzecz pokrzywdzonego, Polskiego Czerwonego Krzyża albo na inny cel społeczny wskazany przez pokrzywdzonego.
	',
	
	'why207' => '
		Art.  207.  [Znęcanie się]
		§  1. 
		Kto znęca się fizycznie lub psychicznie nad osobą najbliższą lub nad inną osobą pozostającą w stałym lub przemijającym stosunku zależności od sprawcy,
		podlega karze pozbawienia wolności od 3 miesięcy do lat 5.

		§  1a. 
		Kto znęca się fizycznie lub psychicznie nad osobą nieporadną ze względu na jej wiek, stan psychiczny lub fizyczny,
		podlega karze pozbawienia wolności od 6 miesięcy do lat 8.

		§  2. 
		Jeżeli czyn określony w § 1 lub 1a połączony jest ze stosowaniem szczególnego okrucieństwa, sprawca
		podlega karze pozbawienia wolności od roku do lat 10.

		§  3. 
		Jeżeli następstwem czynu określonego w § 1-2 jest targnięcie się pokrzywdzonego na własne życie, sprawca
		podlega karze pozbawienia wolności od lat 2 do 12.
	',
];
